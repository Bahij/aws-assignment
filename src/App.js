import React from 'react';
import './App.css';
import S3 from 'react-aws-s3';

// AWS account config
const config = {
  bucketName: 'bahij-aws-assignment',
  region: 'eu-central-1',
  accessKeyId: 'AKIATZ244ZRBKNYIBK4C',
  secretAccessKey: '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',
}

// Set config for Client
const ReactS3Client = new S3(config);

// AWS SDK 
const AWS = require('aws-sdk');

// config dynamoDB using account config
const dynamoDb = new AWS.DynamoDB(config);



var upload = (e) => {
  // File name
  let name = e.target.files[0].name;
  // Upload File to S3 bucket
  ReactS3Client.uploadFile(e.target.files[0], name)
  .then( (data) => {
    // setting params for new dynamoDB field (file name)
    var params = {  
      Item: {
        // Primary Key
        'Name' : {
          // string
          S: name     
        }
      }, 
      ReturnConsumedCapacity: "TOTAL", 
      TableName: "Files"
     };

    //Add item to dynamoDB
    dynamoDb.putItem(params, function(err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      else     console.log(data);           // successful response
    });

    alert("Successfully uploaded file");
  })
  .catch((error) => {

    alert(error);

  })
  ;
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Upload a file to S3 Bucket</h1>
        <input id="inp"
              type="file"
              onChange={upload}
       />
      </header>
    </div>
  );
} 

export default App;
